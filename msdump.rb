#
# msdump
#
# Dump token table from Microsoft Basic CP/M
#
# Call as
#   ruby msdump.rb <offset> basic.com
#
# offset depends on Basic version:
#
# master.com 5.2 14-Jul-80    0x13b
# obasic.com 4.51 1977        ?
# mbasic.com 5.21 28-Jul-81   ?
# mbasic-5.21                 0x13a
# mbasic-5.29 28-Jul-85       0x13a
#

keywords = Hash.new

pos = ARGV.shift.to_i(16)
fname = ARGV.shift

File.open(fname) do |f|
  f.seek(pos, :SET)
  state = :begin
  keyval = nil
  ('A'..'Z').each do |letter|
    word = nil

    loop do
      case state
      when :begin
        word = String.new(letter)
        state = :keyword
      when :keyword
        c = f.sysread(1).ord
        break if c == 0 # end of letter list
        if c < 128
          word << c.chr
        else
          c -= 128
          word << c.chr
          state = :end
        end
      when :end
        keyval = f.sysread(1).ord
        d = keywords[keyval]
        STDERR.puts "Dup 0x#{keyval.to_s(16)}:#{d.inspect}:#{word.inspect}" if d
        keywords[keyval] = word
        state = :begin
      else
        raise "Unknown state #{state.inspect}"
      end
    end # keyword loop
    state = :begin
#    break # exit after first letter
  end # letter loop
  (128..254).each do |key|
    word = keywords[key]
    print "#{key.to_s(16)}:#{keywords[key].inspect}, " if keywords[key]
  end
  puts
  (0..127).each do |key|
    word = keywords[key]
    print "#{keywords[key].inspect}, " if keywords[key]
  end
end # file open
